import { baseUrl, imgUrl, siteUrl } from '../env';

export const BaseUrl = baseUrl;
export const SiteUrl = siteUrl;
export const ImgBaseUrl = imgUrl;
export const HttpTimeOut = 60000;