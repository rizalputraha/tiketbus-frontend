import { appGet, appPost } from '../caller';

const createJamTrayek = async (token, data) => {
    let url = 'api/v1/jam_trayek/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listJamTrayek = async (token) => {
    let url = `api/v1/jam_trayek/read`
    let headers = {'Accept-Encoding':'gzip'};
    let resp = appGet({ url, token, headers });
    return resp;
}

const getByIdJamTrayek = async (token,id) => {
    let url = `api/v1/jam_trayek/read/${id}`
    let headers = {'Accept-Encoding':'gzip'};
    let resp = appGet({ url, token, headers });
    return resp;
}

const updateJamTrayek = async (token, id, data) => {
    let url = `api/v1/jam_trayek/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const deleteJamTrayek = async (token, id) => {
    let url = `api/v1/jam_trayek/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

export default { createJamTrayek, listJamTrayek, updateJamTrayek, deleteJamTrayek,getByIdJamTrayek };