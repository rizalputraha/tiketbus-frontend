import { appGet, appPost } from '../caller';

const createKota = async (token, data) => {
    let url = 'api/v1/kota/create'
    let resp = appPost({ url, data, token });
    return resp;
}

const listKota = async (token) => {
    let url = 'api/v1/kota'
    let resp = appGet({ url, token });
    return resp;
}

const updateKota = async (token, id, data) => {
    let url = `api/v1/kota/update/${id}`
    let resp = appPost({ url, data, token });
    return resp;
}

const deleteKota = async (token, id) => {
    let url = `api/v1/kota/delete/${id}`
    let resp = appPost({ url, token });
    return resp;
}

export default { createKota, updateKota, deleteKota, listKota };